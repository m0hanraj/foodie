//
//  RegistrationViewController.swift
//  Foodie
//
//  Created by SaranRaj on 04/11/20.
//  Copyright © 2020 Twilight. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {

    static let identifier = "RegistrationViewController"
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnRegistration: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onLogin(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func doRegister(phnNumber : String){
        Users.registerNewUser(phnNumber: phnNumber) { (user) in
            if let currentUser = user{
                loggedUser = currentUser
                self.pushToHome()
            }else{
                print("Show Error")
            }
        }
    }
    
    @IBAction func onRegistration(_ sender: UIButton) {
        
    }
    
    private func pushToHome(){
        if let viewController =  self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
               {
                   self.navigationController?.pushViewController(viewController, animated: true)
               }
    }

}
