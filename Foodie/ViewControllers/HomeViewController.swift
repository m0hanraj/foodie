//
//  HomeViewController.swift
//  Foodie
//
//  Created by SaranRaj on 04/11/20.
//  Copyright © 2020 Twilight. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK:- IBOutlet and Variables
    static let identifier = "HomeViewController"
    @IBOutlet weak var lblAmount: UILabel!
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    
   
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

//MARK:- CAll Back
extension HomeViewController{
    @IBAction func onMenu(_ sender: Any) {
        showOptions()
    }
}

//MARK:- Private Method
extension HomeViewController{
    private func showWalletAmount(){
         lblAmount.text = String(format: "%.2f", loggedUser?.wallet_amount ?? 0.0)
     }
     private func initialSetUp(){
         self.title = "Home"
         showWalletAmount()
     }
    private func showOptions(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Send Money", style: .default , handler:{ (UIAlertAction)in
            self.showTransactionAlert()
        }))
        
        alert.addAction(UIAlertAction(title: "Transaction List", style: .default , handler:{ (UIAlertAction)in
            self.pushToTrnasactonList()
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .destructive , handler:{ (UIAlertAction)in
            self.doLogout()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("Cancel")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    private func doLogout(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func pushToTrnasactonList(){
        if let trans = self.storyboard?.instantiateViewController(identifier: TransactionViewController.identifier) as? TransactionViewController{
            self.navigationController?.pushViewController(trans, animated: true)
        }
    }
    
    private func showTransactionAlert(){
        let alertController = UIAlertController(title: "Send Money", message: nil, preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Phone Number"
            textField.keyboardType = .numberPad
            textField.tag = 1
            textField.delegate = self
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Amount"
            textField.keyboardType = .numberPad
            textField.tag = 2
            textField.delegate = self
        }
        
        let saveAction = UIAlertAction(title: "Send", style: .default, handler: { alert -> Void in
            let txtMobileNumber = alertController.textFields![0] as UITextField
            let txtAmount = alertController.textFields![1] as UITextField
            if let number = txtMobileNumber.text, let amount = txtAmount.text{
                self.doSendMoney(number: number, amount: amount)
            }
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action : UIAlertAction!) -> Void in })
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func doSendMoney(number: String, amount: String){
        if isValidate(number: number, amount: amount){
            if let walletAmount = loggedUser?.wallet_amount, let sendAmount = Double(amount){
                loggedUser?.wallet_amount = walletAmount - sendAmount
                loggedUser?.updateDB(completion: { (sucess) in
                    self.showWalletAmount()
                })
                
                if let senderUser = Users.getUserFrom(mobileNumber: number){
                    senderUser.wallet_amount = senderUser.wallet_amount + sendAmount
                    senderUser.updateDB { (sucess) in
                        
                    }
                }
                loggedUser?.newTransAction(toNumber: number, amount: amount, completion: { (success) in
                    if success{
                        AlertManager.showAlert(msg: "Your transcation has been sucessfully completed.")
                    }else {
                        AlertManager.showAlert(msg: "Your transcation has been failed, kindly try again later.")
                    }
                })
            }
        }
    }
    
    
    
    private func isValidate(number: String, amount: String)->Bool{
        if number.count == 0{
            AlertManager.showAlert(msg: "Phone number cannot be empty!")
            return false
        }else if number.count < 10{
            AlertManager.showAlert(msg: "Phone number must be 10 digit")
            return false
        }else if Users.getUserFrom(mobileNumber: number) == nil{
            AlertManager.showAlert(msg: "Recipent is not registered with our system!")
            return false
        }else if amount.count == 0{
            AlertManager.showAlert(msg: "Amount cannot be empty")
            return false
        }else if !validateAmount(amount: amount){
            AlertManager.showAlert(msg: "Insuffecient balance in your wallet!")
            return false
        }else if Double(amount) ?? 0 <= 0 {
            AlertManager.showAlert(msg: "Invalid amount!")
            return false
        }
        return true
    }
    
    private func validateAmount(amount : String)->Bool{
        if let walletAmount = loggedUser?.wallet_amount, let sendAmount = Double(amount){
            return walletAmount > sendAmount
        }
        return false
    }
}

//MARK:- UITextFieldDelegate
extension HomeViewController : UITextFieldDelegate{
    
}
