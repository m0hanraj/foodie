//
//  LoginViewController.swift
//  Foodie
//
//  Created by SaranRaj on 04/11/20.
//  Copyright © 2020 Twilight. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: Outlets and Internal Properties
    static let identifier = "LoginViewController"
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
}

//MARK: SetUp UI
extension LoginViewController
{
     private func initialSetUp() {
     }
}

//MARK: Call Back
extension LoginViewController
{
    
    @IBAction func onLogin(_ sender: UIButton) {
       
        if let user =  Users.getUserFrom(mobileNumber: self.txtPhoneNumber.text ?? ""){
            loggedUser = user
            pushToHome()
        }else{
            print("Not Registered")
        }

    }
    
    @IBAction func onRegistration(_ sender: UIButton) {
        
    }
    
    
}

//MARK: Public Methods
extension LoginViewController
{
    
}

//MARK: Private Methods
extension LoginViewController
{
 
    private func pushToHome(){
        if let viewController =  self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
               {
                   self.navigationController?.pushViewController(viewController, animated: true)
               }
    }
    
}

