//
//  TransactionViewController.swift
//  Foodie
//
//  Created by Saravanan on 04/11/20.
//  Copyright © 2020 Saro. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController {
    
    //MARK:- IBOutlet and Variables
    static let identifier = "TransactionViewController"
    var arrTransactions :[Transactions]?
    @IBOutlet weak var tblVW: UITableView!
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
}

//MARK:- Private Method
extension TransactionViewController {
    private func initialSetup(){
        registerTblVwCell()
        setDelegate()
        getTansActions()
    }
    
    private func getTansActions(){
        self.arrTransactions = loggedUser?.getTransActions()
        tblVW.reloadData()
    }
    
    private func setDelegate(){
        tblVW.delegate = self
        tblVW.dataSource = self
    }
    
    private func registerTblVwCell() {
        self.tblVW.register(UINib(nibName: TCellTransactions.CellIdentifier, bundle: nil), forCellReuseIdentifier: TCellTransactions.CellIdentifier)
    }
}

//MARK:- UITableViewDelegate
extension TransactionViewController : UITableViewDelegate{
    
}

//MARK:- UITableViewDataSource
extension TransactionViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (arrTransactions?.count ?? 0) == 0 {
            tableView.setEmptyMessage("No Transcation done yet!")
        } else {
            tableView.restore()
        }
        return arrTransactions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TCellTransactions.CellIdentifier) as? TCellTransactions else{
          return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.updateCell(trans: arrTransactions?[indexPath.row])
        return cell
    }
}
