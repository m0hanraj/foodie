//
//  CRNotificaton.swift
//  RightNow
//
//  Created by Mohanraj on 05/05/20.
//  Copyright © 2020 TwilightITSolution. All rights reserved.
//

import Foundation
import UIKit


class AlertManager{
    
    class func showAlert(msg: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Foodie", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            alert.view.tintColor = AppColor.app_green[.systemBlue]
            let keyWindow = UIApplication.shared.connectedScenes
                    .filter({$0.activationState == .foregroundActive})
                    .map({$0 as? UIWindowScene})
                    .compactMap({$0})
                    .first?.windows
                .filter({$0.isKeyWindow}).first
            keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    
}
