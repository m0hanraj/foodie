//
//  JSONReader.swift
//  FirebasePhone
//
//  Created by Keshika on 10/07/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation

enum JSONError: Error {
    case noValidURL
    case noValidJSON
}
