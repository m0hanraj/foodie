//
//  Configuration.swift
//  WinqBid
//
//  Created by SaranRaj on 10/12/19.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift
import UIKit

enum InitialScreen : Int {
    case Login
    case Home
}

class AppConfig: NSObject {
    
    static let shared = AppConfig()
    
    public func setUpAPP(){
        
        NetworkConnectionManager.shared.observeReachability()
        UIApplication.shared.windows.last?.window?.tintColor = AppInfo.themeColor
        self.iqKeyboardManagerSetUp()
    }
    
    func iqKeyboardManagerSetUp(color : UIColor = AppInfo.themeColor)
    {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = .white
        IQKeyboardManager.shared.toolbarBarTintColor = color
        IQKeyboardManager.shared.placeholderColor = .white
        IQKeyboardManager.shared.placeholderFont = AppFont.PoppinsRegular[12]
    }

    
    public func loadInitialScreen() {
        let isLogin = UserDefaults().getData(key: UserDefaultKey.Login.rawValue) as? Bool ?? false
        if isLogin == true
        {
            self.setUpInitialControllder(screen: .Home)
        }
        else
        {
            self.setUpInitialControllder(screen: .Login)
        }
    }
    
    public func setUpInitialControllder(screen : InitialScreen) {
        
        var viewController : UIViewController?
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        switch screen {
        case .Login:
            viewController =  storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            break
        case .Home:
            viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            break
        }
        
        if let viewController = viewController
        {
            UIApplication.setRootViewController(targetVc: viewController)
        }
        
    }
    
}
