//
//  Constants.swift
//  LNDR
//
//  Created by SaranRaj on 11/06/19.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit

var sceneDelegate: SceneDelegate? {
    return UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
}

var isNetworkReach = true

//MARK:-App Info
struct AppInfo {
    static let appName = "Doctor"
    static let themeColor = AppColor.app_green[.black]
}

//MARK:-App Color
enum AppColor : String {
    case app_green = "37BA76"
    
    subscript(defaultColor : UIColor) -> UIColor {
        get {
            return UIColor(hex: self.rawValue) ?? defaultColor
        }
    }
    
}

//MARK:-App Font
enum AppFont : String {
    case PoppinsRegular = "Poppins-Regular"
    case PoppinsMedium = "Poppins-Medium"
    subscript(size : CGFloat) -> UIFont {
        get {
            return UIFont(name: self.rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
        }
    }
}

//MARK:-Screen Bounds
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
