//
//  MethodHelper.swift
//  LNDR
//
//  Created by Keshika on 10/07/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import Alamofire

/*** API Router Enum to prepare URL Request ***/
public enum APIRouter : URLRequestConvertible {
    
    case Login

    /*** Base URL for API ***/
    var BaseURL : String {
        return "https://www.winqbid.com/dev/tripBay/api/"
    }
    
    /*** Request Headers ***/
    var headers: Dictionary<String, String>? {
            switch self {
            case .Login:
                return [
                    "Content-Type": "application/json"
                ]
            }
    }
    
    /*** Request Body ***/
    var httpBody : Data? {
        return nil
    }
    
    /*** HTTPMethod Type ***/
    var method: HTTPMethod {
        switch self {
        case .Login:
            return .post
        }
    }
    
    /*** Endpoint Path ***/
    var path: String {
        switch self {
        case .Login:
            return "User/userLogin"
        }
    }
    
    /*** Parameters to pass in request ***/
    var parameters: Dictionary<String, Any>? {
        switch self {
        case .Login:
            return nil
        }
    }
    
    /*** URL encoding for each router ***/
    var encoding: ParameterEncoding{
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    /*** URL request to Call API ***/
    public func asURLRequest() throws -> URLRequest {
        var request = URLRequest(url: try self.BaseURL.asURL().appendingPathComponent(path))
        request.httpMethod = method.rawValue
        request.timeoutInterval = 60
        request.allHTTPHeaderFields = headers
        return try encoding.encode(request, with: parameters)
    }
}

