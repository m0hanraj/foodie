//
//  MethodHelper.swift
//  LNDR
//
//  Created by Keshika on 10/07/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

public enum ErrorsToThrow: Error,LocalizedError {
    case failed
    case NoInternet
    
    public var errorDescription: String? {
        switch self {
        case .NoInternet:
            return NSLocalizedString("Please check your internet connection or try again later.", comment: "Network Issue")
        default :
            return NSLocalizedString("Internal server error!", comment: "Network Issue")
        }
    }
}

class APIManager : NSObject {
    
    /*** Singleton APIManager object to be used throughout the app ***/
    static let shared = APIManager()
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?) -> Void
    private var isRefreshing = false
    
    public func requestAPI<T : Decodable>(router : APIRouter, completion: @escaping (Swift.Result<T, Error>) -> (Void)) {
        
        if !isNetworkReach{
            completion(.failure(ErrorsToThrow.NoInternet))
            return
        }
        print("Url --->",router.urlRequest?.description ?? "No URL")
        print("Header --->",router.headers ?? "No Header")
        if let body = router.httpBody {
            
            print("Body --->",body.toJSON() ?? "No Body")
        }
        print("Params --->",router.parameters ?? "No Params")
        
        AF.request(router).validate().responseDecodable { (response : AFDataResponse<T>) in
            DispatchQueue.main.async {
                
                if let responseData = response.data {
                    print("Response --->",responseData.toJSON() ?? "")
                    JSONResponseDecoder.decodeFrom(responseData, returningModelType: T.self, completion: { (model, error) in
                        DispatchQueue.main.async {
                            if let parserError = error {
                                completion(.failure(parserError))
                                return
                            }
                            if let model = model {
                                completion(.success(model))
                                return
                            }
                        }
                    })
                } else if let parserError = response.error {
                    completion(.failure(parserError))
                    return
                }
            }
        }
    }
    
}

