//
//  UIViewController+RightNow.swift
//  Promade
//
//  Created by SaranRaj on 01/04/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    @objc func onLeftButton(_ sender : Any)  {
        
    }
    
    @objc func onRightButton(_ sender : Any)  {
        
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
}

