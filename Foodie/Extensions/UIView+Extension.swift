//
//  UIView+Extension.swift
//  WinqBid
//
//  Created by Keshika on 10/07/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

enum BorderPostion : Int {
     case Left = 0
     case Top
     case Right
     case Bottom
 }

extension UIView{
    
    func rootView() -> UIView {
        return superview?.rootView() ?? superview ?? self
    }
    
    var globalPoint :CGPoint? {
        return self.superview?.convert(self.frame.origin, to: nil)
    }

    var globalFrame :CGRect? {
        return self.superview?.convert(self.frame, to: nil)
    }
    
    func addBorderLine(color: UIColor, width: CGFloat,position : BorderPostion) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        switch position {
        case .Left:
            border.name = "leftLayer"
            border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        case .Top:
            border.name = "topLayer"
            border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        case .Right:
            border.name = "rightLayer"
            border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        case .Bottom:
            border.name = "bottomLayer"
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        }
        self.layer.addSublayer(border)
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11, *) {
            var cornerMask = CACornerMask()
            if(corners.contains(.topLeft)){
                cornerMask.insert(.layerMinXMinYCorner)
            }
            if(corners.contains(.topRight)){
                cornerMask.insert(.layerMaxXMinYCorner)
            }
            if(corners.contains(.bottomLeft)){
                cornerMask.insert(.layerMinXMaxYCorner)
            }
            if(corners.contains(.bottomRight)){
                cornerMask.insert(.layerMaxXMaxYCorner)
            }
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = cornerMask
            self.layer.masksToBounds = true

        } else {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
    
}
