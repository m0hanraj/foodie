//
//  Data+Promade.swift
//  Techmah-Promade
//
//  Created by Keshika on 10/07/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit

extension Data
{
    func toJSON() -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
}
