//
//  UserDefaults+WinqBid.swift
//  WinqBid
//
//  Created by SaranRaj on 19/12/19.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit

enum UserDefaultKey : String
{
    case PhoneNumber = "PhoneNumber" // Type - String
    case Login = "Login" // Type - Bool
}

extension UserDefaults
{
    
    func setData(key : String,value : Any)
    {
        set(value, forKey: key)
    }
    
    func getData(key : String) -> Any?
    {
        return object(forKey: key)
    }
    
}
