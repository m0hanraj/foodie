//
//  UItextField+Extension.swift
//  WinqBid
//
//  Created by SaranRaj on 12/12/19.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit
private var kAssociationKeyMaxLength: Int = 0

@IBDesignable
extension UITextField {
    
    public func isNotEmptyTextfield() -> Bool
    {
        if self.text?.length() == 0
        {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            return false
        }
        return true
    }
    
    func addLeftView(leftImage : UIImage?,controller : UIViewController?) {
        self.leftViewMode = UITextField.ViewMode.always
        let leftButton = UIButton(type: .custom)
        leftButton.tag = 144
        if let image = leftImage {
            leftButton.setImage(image, for: .normal)
        }
        leftButton.frame = CGRect(x: 0, y: 0, width: self.frame.size.height, height: self.frame.size.height)
        leftButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        leftButton.imageView?.contentMode = .scaleAspectFit
        leftButton.isUserInteractionEnabled = true
        if let vc = controller
        {
            leftButton.addTarget(vc, action: #selector(vc.onLeftButton), for: .touchUpInside)
        }
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.height, height: self.frame.height))
        vw.addSubview(leftButton)
        leftButton.frame = vw.frame
        leftButton.center = vw.center
        self.leftView = vw
        self.leftViewMode = UITextField.ViewMode.always
    }
    
    func addRightView(rightImage : UIImage?,controller : UIViewController?) {
        let rightButton = UIButton(type: .custom)
        rightButton.tag = 143
        if let image = rightImage{
            rightButton.setImage(image, for: .normal)
        }
        rightButton.frame = CGRect(x: 0, y: 0, width: self.frame.size.height, height: self.frame.size.height)
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        rightButton.imageView?.contentMode = .scaleAspectFit
        rightButton.isUserInteractionEnabled = true
        if let vc = controller
        {
            rightButton.addTarget(vc, action: #selector(vc.onRightButton(_:)), for: .touchUpInside)
        }
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.height, height: self.frame.height))
        vw.addSubview(rightButton)
        rightButton.frame = vw.frame
        rightButton.center = vw.center
        self.rightView = vw
        self.rightViewMode = UITextField.ViewMode.always
    }
    
    @IBInspectable var PlaceholderColor: UIColor? {
        get {
            return self.PlaceholderColor ?? self.textColor ?? .white
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",attributes: [NSAttributedString.Key.foregroundColor: newValue ?? self.textColor ?? .white])
        }
    }
    
    @IBInspectable var LeftPadding: CGFloat {
        get {
            return leftView?.frame.size.width ?? 0.0
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }
    
    @IBInspectable var RightPadding: CGFloat {
        get {
            return rightView?.frame.size.width ?? 0.0
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
    
    @IBInspectable var maxLength: Int {
          get {
              if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                  return length
              } else {
                  return Int.max
              }
          }
          set {
              objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
              addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
          }
      }

      @objc func checkMaxLength(textField: UITextField) {
          guard let prospectiveText = self.text,
              prospectiveText.count > maxLength
              else {
                  return
          }

          let selection = selectedTextRange

          let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
          let substring = prospectiveText[..<indexEndOfText]
          text = String(substring)

          selectedTextRange = selection
      }
    
}

