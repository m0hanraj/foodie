//
//  UINavigationController+WinqBid.swift
//  WinqBid
//
//  Created by Keshika on 10/07/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController{
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureNavigationBar(largeTitleColor: UIColor, backgoundColor: UIColor, tintColor: UIColor, title: String?, preferredLargeTitle: Bool,backgroundImage : UIImage? = nil) {
        
        self.navigationBar.largeTitleTextAttributes = [.foregroundColor: largeTitleColor,.font : AppFont.PoppinsMedium[20]]
        self.navigationBar.titleTextAttributes = [.foregroundColor: largeTitleColor,.font : AppFont.PoppinsMedium[20]]
        self.navigationBar.barTintColor = tintColor
        self.navigationBar.tintColor = tintColor
        self.navigationBar.isTranslucent = true
        self.navigationBar.backgroundColor = backgoundColor
        self.topViewController?.title = title
        
        if let image = backgroundImage
        {
            self.navigationBar.setBackgroundImage(image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        }
        else{
            self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationBar.shadowImage = UIImage()
        }
        
        if #available(iOS 13.0, *) {
            self.navigationBar.prefersLargeTitles = preferredLargeTitle
        } else {
            // Fallback on earlier versions
        }
    }
    
    func popToVC(vc: UIViewController.Type) -> Bool {
        var isPushed = false
        for controller in viewControllers {
            if controller.isKind(of: vc) {
                isPushed = true
                popToViewController(controller, animated: true)
                break
            }
        }
        return isPushed
    }
    
}
