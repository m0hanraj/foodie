//
//  String+RightNow.swift
//  Promade
//
//  Created by Keshika on 10/07/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit

extension String{
    
    /** String to URLRequest **/
    func asURLRequest()->URLRequest?{
        var urlRequest:URLRequest?
        if self.isValidForUrl(){
            if let url = URL(string: self){
                urlRequest = URLRequest(url: url)
            }
        }else{
            urlRequest = nil
        }
        return urlRequest
    }
    
    /** validate URL **/
    func isValidForUrl() -> Bool {
        
        if(self.hasPrefix("http") || self.hasPrefix("https")) {
            return true
        }
        return false
    }
    
    /** Get actual String after removing white spaces and new lines **/
    public func trim() -> String{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    /** Get actual String length after removing white spaces and new lines  **/
    public func length() -> Int{
        return self.isEmpty ? 0 : self.trim().count
    }
    
    /** Email Validation **/
    func isValidEmail() -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    /** Convert String to Dictionary **/
    func toDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
