//
//  UIApplication+RightNow.swift
//  Promade
//
//  Created by SaranRaj on 31/03/20.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import  UIKit


extension UIApplication {
    
    class func topViewController(base: UIViewController? = UIApplication.shared.windows.first{$0.isKeyWindow}?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    class func setRootViewController(targetVc : UIViewController){
        let nav = UINavigationController(rootViewController: targetVc)
        nav.configureNavigationBar(largeTitleColor: .orange, backgoundColor: .clear, tintColor: .orange, title: nil, preferredLargeTitle: false)
        sceneDelegate?.window?.rootViewController = nav
        sceneDelegate?.window?.makeKeyAndVisible()
    }
    
}
