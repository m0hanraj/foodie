//
//  ConnectionManager.swift
//  LNDR
//
//  Created by SaranRaj on 11/07/19.
//  Copyright © 2020 SaranRaj. All rights reserved.
//

import Foundation
import UIKit
import Reachability

class NetworkConnectionManager {
    
    static let shared = NetworkConnectionManager()
    private var reachability : Reachability!
    
    func observeReachability(){
        self.reachability = try? Reachability()
        NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        do {
            try self.reachability.startNotifier()
        }
        catch(let error) {
            print("Error occured while starting reachability notifications : \(error.localizedDescription)")
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .cellular:
            isNetworkReach = true
            break
        case .wifi:
            isNetworkReach = true
            break
        case .none:
            isNetworkReach = false
            break
        case .unavailable:
            isNetworkReach = false
        }
    }
}
