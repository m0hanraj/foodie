//
//  Constants.swift
//  Foodie
//
//  Created by Saravanan on 04/11/20.
//  Copyright © 2020 Saro. All rights reserved.
//

import Foundation

var loggedUser: Users?
var fourDigitNumber: String {
    var result = ""
    repeat {
        // Create a string with a random number 0...9999
        result = String(format:"%04d", arc4random_uniform(10000) )
    } while result.count < 4
    return result
   }
