//
//  User.swift
//  Foodie
//
//  Created by Saravanan on 04/11/20.
//  Copyright © 2020 Saro. All rights reserved.
//

import Foundation
import UIKit
import CoreData

enum UserEntity : String{
    case phoneNumber = "phone_number"
    case roleID = "role_id"
    case walletAmount = "wallet_amount"
}

extension Users{
    
    func updateDB(completion: @escaping(Bool) -> ()){
        CoreDataHelper.saveDB { (success) in
            completion(success)
        }
    }
    class func getUserFrom(mobileNumber : String)->Users?{
        let request: NSFetchRequest<Users> = Users.fetchRequest()
        request.predicate = NSPredicate(format: "\(UserEntity.phoneNumber.rawValue) = \(mobileNumber)")
        do {
            let users = try CoreDataHelper.getManagedContext().fetch(request)
            if users.count > 0{
                return users.first
            }
        }  catch {
           return nil
        }
        return nil
    }
    class func registerNewUser(phnNumber : String,completion: @escaping(Users?) -> ()){
        guard getUserFrom(mobileNumber: phnNumber) == nil else{
            completion(nil)
            return
        }
        let userMangedObject = Users(context: CoreDataHelper.getManagedContext())
        userMangedObject.phone_number = phnNumber
        userMangedObject.role_id = 2
        userMangedObject.wallet_amount = 1000.0
        CoreDataHelper.saveDB { (status) in
            if status{
                completion(userMangedObject)
            }else{
                completion(nil)
            }
        }
    }
    func getTransActions()->[Transactions]?{
        guard let phnNum = self.phone_number else{
            return nil
        }
       return Transactions.getTransactionsFrom(phnNum: phnNum)
    }
    
    func newTransAction(toNumber : String, amount : String,completion: @escaping(Bool) -> ()){
        guard let phnNum = self.phone_number else{
            completion(false)
            return 
        }
        Transactions.newTransaction(senderNum: phnNum, reciverNum: toNumber, amount: amount) { (success) in
            completion(success)
        }
    }
}
