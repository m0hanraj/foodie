//
//  Tansactions.swift
//  Foodie
//
//  Created by Saravanan on 04/11/20.
//  Copyright © 2020 Saro. All rights reserved.
//

import Foundation
import UIKit
import CoreData

enum TransactionType : Int{
    case credit = 1
    case debit = 2
}
extension Transactions{
   
    class func getAllTransactions()->[Transactions]?{
        let request: NSFetchRequest<Transactions> = Transactions.fetchRequest()
        do {
            let trans = try CoreDataHelper.getManagedContext().fetch(request)
            return trans
        }  catch {
           return nil
        }
    }
    
    class func getTransactionsFrom(phnNum : String)->[Transactions]?{
          let request: NSFetchRequest<Transactions> = Transactions.fetchRequest()
          request.predicate = NSPredicate(format: "\(TransactionEntity.receiverNumber.rawValue) = \(phnNum) OR \(TransactionEntity.senderNumber.rawValue) = \(phnNum)")
          do {
              let trans = try CoreDataHelper.getManagedContext().fetch(request)
              return trans
          }  catch {
             return nil
          }
      }
    
    class func newTransaction(senderNum : String,reciverNum : String,amount : String,completion: @escaping(Bool) -> ()){
        
        let transMangedObject = Transactions(context: CoreDataHelper.getManagedContext())
        transMangedObject.sender_number = senderNum
        transMangedObject.receiver_number = reciverNum
        transMangedObject.amount = Double(amount) ?? 0.0
        transMangedObject.trans_id = "TRNX\(fourDigitNumber)"
        
        CoreDataHelper.saveDB { (status) in
            completion(status)
        }
    }
    
}
