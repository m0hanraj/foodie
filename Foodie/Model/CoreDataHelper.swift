//
//  CoreDataHelper.swift
//  Foodie
//
//  Created by Saravanan on 03/11/20.
//  Copyright © 2020 Saro. All rights reserved.
//

import Foundation
import CoreData
import UIKit

enum Table: String{
    case users = "Users"
    case transactions = "Transactions"
}

enum TransactionEntity : String{
    case amount = "amount"
    case receiverNumber = "receiver_number"
    case senderNumber = "sender_number"
    case transId = "trans_id"
    case type = "type"
}






class CoreDataHelper: NSObject {
    
    class func getManagedContext()->NSManagedObjectContext{
   
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    class func getManagedObjectFrom(name : String)->NSManagedObject?{
        guard let entity = NSEntityDescription.entity(forEntityName: name, in: CoreDataHelper.getManagedContext())else{
            return nil
        }
        return NSManagedObject(entity: entity, insertInto: CoreDataHelper.getManagedContext())
    }
    
    class func saveDB(completion: @escaping(Bool) -> ()){
        do {
            try CoreDataHelper.getManagedContext().save()
            completion(true)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            completion(false)
        }
    }
    
}
