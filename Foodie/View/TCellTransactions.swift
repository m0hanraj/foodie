//
//  TCellTransactions.swift
//  Foodie
//
//  Created by Saravanan on 04/11/20.
//  Copyright © 2020 Saro. All rights reserved.
//

import UIKit

class TCellTransactions: UITableViewCell {
    static let CellIdentifier = "TCellTransactions"
    @IBOutlet weak var lblTransID: UILabel!
    @IBOutlet weak var lblReceiverNumber: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension TCellTransactions {
    func updateCell(trans : Transactions?){
        lblTransID.text = trans?.trans_id
        lblReceiverNumber.text = trans?.receiver_number
        lblAmount.text = "\(trans?.amount ?? 0.0)"
        if isCredit(receiverNum: lblReceiverNumber.text) {
            lblType.text = "Credit"
        } else {
            lblType.text = "Debit"

        }
    }
    
    func isCredit(receiverNum : String?)->Bool{
        return receiverNum == loggedUser?.phone_number
    }
}
